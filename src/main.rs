#![no_std]
#![no_main]

use cortex_m_rt::entry;
use embedded_graphics::{
    fonts::{Font24x32 as Font, Text},
    image::Image,
    pixelcolor::BinaryColor,
    prelude::*,
    primitives::{Circle, Line, Triangle},
    style::{PrimitiveStyle, PrimitiveStyleBuilder, TextStyleBuilder},
};
use embedded_hal::digital::v2::OutputPin;
use embedded_time::fixed_point::FixedPoint;
use embedded_time::rate::Extensions;
use epd_waveshare::{
    color::*, epd7in5_v2::Display7in5 as EPDisplay, epd7in5_v2::EPD7in5 as EPD, epd7in5_v2::HEIGHT,
    epd7in5_v2::WIDTH, graphics::DisplayRotation, prelude::*,
};
use panic_probe as _;
use rp2040_hal as hal;
use tinytga::Tga;

use hal::{
    clocks::{init_clocks_and_plls, Clock},
    pac,
    sio::Sio,
    watchdog::Watchdog,
};

// IMPORTANT!! first of all, you need to load the BOOT2 binary (rp-rs courtesy)
#[link_section = ".boot2"]
#[used]
pub static BOOT2: [u8; 256] = rp2040_boot2::BOOT_LOADER_W25Q080;

#[entry]
fn main() -> ! {
    // Start all the peripherals
    let mut pac = pac::Peripherals::take().unwrap();
    let core = pac::CorePeripherals::take().unwrap();
    let mut watchdog = Watchdog::new(pac.WATCHDOG);
    let sio = Sio::new(pac.SIO);

    let external_xtal_freq_hz = 12_000_000u32;
    let clocks = init_clocks_and_plls(
        external_xtal_freq_hz,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    let mut delay = cortex_m::delay::Delay::new(core.SYST, clocks.system_clock.freq().integer());

    let pins = hal::gpio::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );
    // End of Start the peripherals

    // SPI declaration
    let _spi_sclk = pins.gpio10.into_mode::<hal::gpio::FunctionSpi>();
    let _spi_mosi = pins.gpio11.into_mode::<hal::gpio::FunctionSpi>();
    let spi = hal::spi::Spi::<_, _, 8>::new(pac.SPI1);

    let mut spi = spi.init(
        &mut pac.RESETS,
        clocks.peripheral_clock.freq(),
        // you can put cookie (increase the speed) in it but I don't recommend it.
        4_000_000u32.Hz(),
        &embedded_hal::spi::MODE_0,
    );
    // End of SPI declaration

    // Start the rest of pins needed to communicate with the screen
    let mut cs = pins.gpio9.into_push_pull_output(); // CS
    cs.set_high().unwrap();
    let busy = pins.gpio13.into_pull_up_input(); // BUSY
    let dc = pins.gpio8.into_push_pull_output(); // DC
    let rst = pins.gpio12.into_push_pull_output(); // RST

    // Start the EPD struct
    let mut epd = EPD::new(
        &mut spi,   // SPI
        cs,         // CS
        busy,       // BUSY
        dc,         // DC
        rst,        // RST
        &mut delay, // DELAY
    )
    .unwrap();
    // Start the display buffer
    let mut display = EPDisplay::default();

    epd.wake_up(&mut spi, &mut delay).unwrap();
    // epd.set_background_color(epd_waveshare::color::Color::Black);
    // epd.clear_frame(&mut spi).unwrap();
    // epd.set_background_color(epd_waveshare::color::Color::White);
    epd.clear_frame(&mut spi).unwrap();

    display.clear_buffer(Color::Black);

    // Start the fun
    let style_text = TextStyleBuilder::new(Font)
        .text_color(BinaryColor::Off)
        .background_color(BinaryColor::On)
        .build();
    let style_forms = PrimitiveStyleBuilder::new()
        .stroke_width(10)
        .stroke_color(BinaryColor::Off)
        .fill_color(BinaryColor::Off)
        .build();

    for i in 0..(WIDTH / 10) as i32 {
        Line::new(Point::new(i * 10, 0), Point::new(i * 10, HEIGHT as i32))
            .into_styled(PrimitiveStyle::with_stroke(BinaryColor::Off, 1))
            .draw(&mut display)
            .unwrap();
    }
    for i in 0..(HEIGHT / 10) as i32 {
        Line::new(Point::new(0, i * 10), Point::new(WIDTH as i32, i * 10))
            .into_styled(PrimitiveStyle::with_stroke(BinaryColor::Off, 1))
            .draw(&mut display)
            .unwrap();
    }

    display.set_rotation(DisplayRotation::Rotate270);

    let tga = Tga::from_slice(include_bytes!(concat!(
        env!("CARGO_MANIFEST_DIR"),
        "/RustMX.tga"
    )))
    .unwrap();

    let image: Image<Tga, BinaryColor> = Image::new(&tga, Point::new(50, 100));
    image.draw(&mut display).unwrap();

    Text::new("RustMX", Point::new(50, 50))
        .into_styled(style_text)
        .draw(&mut display)
        .unwrap();

    epd.update_frame(&mut spi, display.buffer()).unwrap();
    epd.display_frame(&mut spi).unwrap();
    delay.delay_ms(1000);

    let mut led_pin = pins.gpio25.into_push_pull_output();

    // If the led blink, everything it's ok (big debug skills)
    loop {
        led_pin.set_high().unwrap();
        delay.delay_ms(500);
        led_pin.set_low().unwrap();
        delay.delay_ms(500);
    }
}
