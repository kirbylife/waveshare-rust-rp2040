# Waveshare-Rust-RP2040
### This is a template to use a waveshare e-ink screen with the rp-pico board andrp-pico HAT using Rust.

## Prepare your system
- Install the toolchain
 - `rustup target install thumbv6m-none-eabi`
- Install the linker (with [flip-link](https://github.com/knurling-rs/flip-link) you will have zero-cost stack overflow)
 - `cargo install flip-link`
- Install elf2uf2-rs
 - `cargo install elf2uf2-rs`
- Install the pico-sdk
 - (search on the Internet, it's really boring and I don't remember how to do it lol)
- Install the gcc-arm corresponding to the board
 - (The same as with pico-sdk... sorry)

## Usage
1. Clone the repo
 - `git clone https://gitlab.com/kirbylife/waveshare-rust-rp2040`
2. Build with --release
 - `cargo build --release`
3. Convert to uf2
 - `elf2uf2-rs target/thumbv6m-none-eabi/release/waveshare_rust_rp2040 binary.uf2`
4. Connect your rp-pico to the PC with in BOOTSEL mode
5. Grab your file `binary.uf2` and drop it inside of the RPI-RP2 volume
6. Enjoy :) (apply restrictions)

## License
This template does not have any license but the [RustMX](https://t.me/RustMX) can have it.

written in Rust the ran originally in the Raspberry Pi Pico with a Waveshare 7.5 inch e-ink screen.
